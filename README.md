# Development

### 1. First time setup
In veera-html directory run:

    npm install


### 2. Running the project
In veera-html directory run:

    gulp


### 3. Develope the project
1. Get veera-styles repo

    git clone https://koodivaramu.eesti.ee/veebiraamistik/veera/veera-styles.git

2. In veera-styles directory run:
    
    npm link

3. In veera-html directory run:

    npm link veera-styles

4. In veera-html directory run:

    gulp

Build styles in veera-styles directory, gulp watches and livereloads the veera-html project.